// #include <iostream>
// #include <vector>
// #include <algorithm>


// int MaxPairwiseProduct(const std::vector<int>& numbers) {
//     int max_product = 0;
//     int n = numbers.size();

//     for (int first = 0; first < n; ++first) {
//         for (int second = first + 1; second < n; ++second) {
//             max_product = std::max(max_product,
//                 numbers[first] * numbers[second]);
//         }
//     }

//     return max_product;
// }

// int main() {
//     int n;
//     std::cin >> n;
//     std::vector<int> numbers(n);
//     for (int i = 0; i < n; ++i) {
//         std::cin >> numbers[i];
//     }

//     int result = MaxPairwiseProduct(numbers);
//     std::cout << result << "\n";
//     return 0;
// }


#include <iostream>
#include <vector>
#include <cstdlib>

int max_pairwise_product_naive(const std::vector<int> &numbers)
{
    int n = numbers.size();
    int prod = 0;
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            if (i != j)
            {
                if (numbers[i] * numbers[j] > prod)
                {
                    prod = numbers[i] * numbers[j];
                }
            }
        }
    }
    return prod;
}

// slightly better beu but still naive
long long max_pairwise_product_naive2(const std::vector<int>& numbers){
    int n = numbers.size();
    long long prod = 0;
    for (int i = 0; i < n; ++i){
        for (int j = i + 1; j < n; ++j){
            prod = std::max(prod, (long long)numbers[i] * numbers[j]);
        }
    }
    return prod;
}

long long max_pairwise_product(const std::vector<int>& numbers){
    int n = numbers.size();
    long long max1 = 0;
    int index1 = 0;
    for (int i = 0; i < n; ++i){
        if (numbers[i] > max1){
            max1 = numbers[i];
            index1 = i;
        }
    }
    long long max2 = 0;
    int index2 = 0;
    for (int i = 0; i < n; ++i){
        if ((numbers[i] > max2) && (i != index1)){
            max2 = numbers[i];
            index2 = i;
        }
    }
    return max1*max2;
}

int main(void)
{
    int x = std::rand() % 10 + 2;
    std::cout << x;
    int n;
    std::cin >> n;
    std::vector<int> numbers(n);
    for (int i = 0; i < n; ++i)
    {
        std::cin >> numbers[i];
    }

    long long max_product = max_pairwise_product(numbers);
    std::cout << max_product;
    return 0;
}
